FROM ruby:3.1.2

# Register Yarn package source.
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN sh -c "echo 'deb https://dl.yarnpkg.com/debian/ stable main' >> /etc/apt/sources.list"

# Install additional packages.
RUN apt update -qq
RUN apt install -y postgresql-client nodejs
RUN apt --no-install-recommends install yarn

# Prepare working directory.
ENV RAILS_ROOT /ror
WORKDIR /ror
COPY ./ror /ror
RUN yarn add bootstrap @popperjs/core jquery @fortawesome/fontawesome-free
RUN gem install bundler
RUN bundle install --full-index

# Configure endpoint.
COPY .docker/ror/entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
EXPOSE 3000
RUN bundle exec rake assets:precompile
# Start app server.
CMD ["bundle", "exec", "puma", "-C", "config/puma.rb"]
