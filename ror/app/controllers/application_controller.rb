class ApplicationController < ActionController::Base
    before_action :set_locale
    
    def set_locale
        I18n.locale = locale || I18n.default_locale
    end

    def locale
        return params[:locale] if ["es", "en"].include? params[:locale]
    end
    
    def page_not_found
    end
    def home
    end
end
