module ApplicationHelper
    def current_page_params
        request.params.slice("locale")
    end
end
