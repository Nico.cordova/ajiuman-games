Rails.application.routes.draw do
  resources :games
  resources :swinging_demo
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html
  # Defines the root path route ("/")
  # root "articles#index"
  get 'brickies/privacy_policy', to:'brickies#privacy_policy'
  get 'brickies/terms_and_conditions', to:'brickies#terms_and_conditions'
  get 'farm_clicker/privacy_policy', to:'farm_clicker#privacy_policy'
  get 'farm_clicker/terms_and_conditions', to:'farm_clicker#terms_and_conditions'
  get 'not_found', to:'application#not_found'
  get 'home', to:'application#home'
  root "application#home"
end
